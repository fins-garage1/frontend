import React, { useState, useEffect, useCallback } from "react";
import { Jumbotron } from "@/components";
import styles from "@/styles/gallery.module.css";
import Zoom from "react-medium-image-zoom";
import Image from "next/image";
import { BsFillCaretRightFill, BsFillCaretLeftFill } from "react-icons/bs";
import data_images from "@/json/image_dummy.json";
import { useQuery } from "@tanstack/react-query";
import { listGalleries } from "@/API/galleriesAPI";

export default function Index() {
  const [page, setPage] = useState(1);
  const limit = 4;
  const divideData = Math.ceil(data_images.length / limit);
  const start = (page - 1) * limit;
  const end = start + limit;

  const nextStart = page * limit;
  const nextEnd = nextStart + limit;
  const nextDataImages = data_images.slice(nextStart, nextEnd);

  const checkPrevData = page > 1;
  const checkNextData = nextDataImages.length > 0;

  const handleNextPage = useCallback(() => {
    if (page && checkNextData) {
      setPage((currentVal) => currentVal + 1);
    }
  }, [page, checkNextData]);

  const handlePrevPage = useCallback(() => {
    if (checkPrevData) {
      setPage((currentVal) => currentVal - 1);
    }
  }, [checkPrevData]);

  const {
    data: galleryData,
    isLoading: galleryLoading,
    error: galleryError,
  } = useQuery(["galleries"], async () => {
    try {
      const response = await listGalleries("yes", 99999999999);
      const result = response;

      return result;
    } catch (err) {
      console.log("Failed to fetch galleries data");
    }
  });

  return (
    <div className="container_page">
      <Jumbotron />

      <div className="p-5">
        <div className="text-center">
          <h1>AKTIVITAS GALERI KAMI</h1>
          <div className="hr_line m-auto"></div>
        </div>

        <div
          data-aos="fade-up"
          data-aos-duration="2000"
          className="d-flex flex-wrap justify-content-center p-4"
        >
          {(galleryData?.data.result.data.data || [])
            .slice(start, end)
            ?.map((item: any, index: number) => (
              <div key={index} className={`m-4 ${styles.square_gallery}`}>
                <Zoom>
                  <Image
                    loader={() =>
                      process.env.NEXT_PUBLIC_BE_URL +
                      "/storage/galleries/" +
                      item.image
                    }
                    src={`${process.env.NEXT_PUBLIC_BE_URL}/storage/galleries/${item.image}`}
                    layout="responsive"
                    width={250}
                    height={250}
                    sizes="100vw"
                    alt="Picture of the author"
                  />
                </Zoom>
              </div>
            ))}
        </div>

        <div className="d-flex justify-content-center p-2">
          <button
            className={`btn m-1 ${
              checkPrevData ? "btn-outline-dark" : "btn-dark"
            } `}
            onClick={handlePrevPage}
          >
            {<BsFillCaretLeftFill />}
          </button>

          {Array.from({ length: divideData }, (_, index) => index + 1).map(
            (item, index) => {
              if (page == item) {
                return (
                  <button
                    className="btn btn-dark m-1"
                    onClick={() => {
                      setPage(item);
                    }}
                    key={index}
                  >
                    {item}
                  </button>
                );
              } else {
                return (
                  <button
                    className="btn btn-outline-dark m-1"
                    onClick={() => {
                      setPage(item);
                    }}
                    key={index}
                  >
                    {item}
                  </button>
                );
              }
            }
          )}

          <button
            className={`btn m-1 ${
              checkNextData ? "btn-outline-dark" : "btn-dark"
            } `}
            onClick={handleNextPage}
          >
            {<BsFillCaretRightFill />}
          </button>
        </div>
      </div>
    </div>
  );
}
