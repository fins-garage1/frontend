import { AppProps } from "next/app";
import "../styles/global-styles.css";
import React, { useEffect } from "react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import "bootstrap/dist/css/bootstrap.min.css";
import { Footer, Navbar } from "@/components";
import "aos/dist/aos.css";
import "react-medium-image-zoom/dist/styles.css";
import * as dotenv from "dotenv";
import { getFileUrlsFromS3 } from "../utils/s3Service";
dotenv.config();

const App = ({ Component, pageProps }: AppProps) => {
  const [queryClient] = React.useState(() => new QueryClient());
  const [dataS3, setDataS3] = React.useState({});

  useEffect(() => {
    const importModule = async () => {
      await import("bootstrap/dist/js/bootstrap.bundle.min.js");
      const AOS = await import("aos");

      AOS.init({
        once: true,
      });
      AOS.refresh();
    };

    async function fetchBuckets() {
      const files = await getFileUrlsFromS3();
      console.log("files", files);
      setDataS3(files);
    }
    fetchBuckets();

    importModule();
  }, []);
  return (
    <>
      <QueryClientProvider client={queryClient}>
        <ReactQueryDevtools />
        <Navbar files={dataS3} />
        <Component {...pageProps} />
        <Footer />
      </QueryClientProvider>
    </>
  );
};

export default App;
