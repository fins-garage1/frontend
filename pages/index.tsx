import { Jumbotron } from "@/components";
import Image from "next/image";
import styles from "@/styles/home.module.css";
import {
  BsSearch,
  BsFillGeoAltFill,
  BsPersonCheckFill,
  BsTools,
  BsFillCaretRightFill,
  BsFillCaretDownFill,
} from "react-icons/bs";
import data_images from "@/json/image_dummy.json";
// import data_avatars from "@/json/avatar_dummy.json";
import Zoom from "react-medium-image-zoom";
import "react-medium-image-zoom/dist/styles.css";
import { useRouter } from "next/router";
import { useEffect, useLayoutEffect, useState } from "react";
import { Carousel } from "react-bootstrap";
import data_collapses from "@/json/list_collapse_question.json";
import Whatsapp from "@/components/Whatsapp";
import { useQuery } from "@tanstack/react-query";
import { listIntroduction } from "@/API/introAPI";
import { listResults } from "@/API/resultsAPI";
import { listGalleries } from "@/API/galleriesAPI";
import { listTestimonies } from "@/API/testimoniesAPI";

const dummy_image1 =
  "https://assets.architecturaldigest.in/photos/600825b81363405bf8eb5086/16:9/w_2560%2Cc_limit/Garage-Rend_03_A-1366x768.jpg";

export default function Index() {
  const [windowSize, setWindowSize] = useState<TWindowSize>({
    height: 0,
    width: 0,
  });
  const [listCollapseQuestions, setListCollapseQuestions] = useState<
    TListCollapseQuestion[]
  >([...data_collapses]);
  const router = useRouter();
  const [index, setIndex] = useState(0);

  const {
    data: introData,
    isLoading: introLoading,
    error: introError,
  } = useQuery(["introduction"], async () => {
    try {
      const response = await listIntroduction("yes");
      const result = response;

      return result;
    } catch (err) {
      console.log("Failed to fetch introduction data");
    }
  });

  const {
    data: resultData,
    isLoading: resultLoading,
    error: resultError,
  } = useQuery(["results"], async () => {
    try {
      const response = await listResults("yes");
      const result = response;

      return result;
    } catch (err) {
      console.log("Failed to fetch results data");
    }
  });

  const {
    data: galleryData,
    isLoading: galleryLoading,
    error: galleryError,
  } = useQuery(["galleries"], async () => {
    try {
      const response = await listGalleries("yes", 10);
      const result = response;

      return result;
    } catch (err) {
      console.log("Failed to fetch galleries data");
    }
  });

  const {
    data: testimoniesData,
    isLoading: testimoniesLoading,
    error: testimoniesError,
  } = useQuery(["testimoni"], async () => {
    try {
      const response = await listTestimonies("yes");
      const result = response;

      return result;
    } catch (err) {
      console.log("Failed to fetch testimonies data");
    }
  });

  useLayoutEffect(() => {
    function handleResize() {
      const widthWindows = window.innerWidth;
      const heightWindows = window.innerHeight;

      setWindowSize({
        width: widthWindows,
        height: heightWindows,
      });
    }

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [windowSize]);

  const CustomZoomInIcon = () => <div>Custom Zoom In Icon</div>;

  const handleSelect = (selectedIndex: any) => {
    setIndex(selectedIndex);
  };

  return (
    <div className="container_page">
      <section id="home">
        <Jumbotron />
      </section>

      <section id="about" className="mt-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div
                data-aos="fade-up"
                data-aos-duration="2000"
                className="d-flex justify-content-center p-4"
              >
                <div className={`${styles.right_introduction}`}>
                  <h1 className="mt-4">PERKENALAN</h1>
                  <div className="hr_line mb-3"></div>
                  <div>
                    <p>{introData ? introData.data.result[0].paragraph : ""}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div
                data-aos="fade-up"
                data-aos-duration="2000"
                className={`${styles.left_introduction} p-4`}
              >
                {introData?.data.result[0].video ? (
                  <iframe
                    width="560"
                    height="315"
                    src={introData?.data.result[0].video}
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowFullScreen
                  ></iframe>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="mt-5">
        <div
          data-aos="fade-up"
          data-aos-duration="2000"
          className={`${styles.our_results_box}`}
        >
          <div className={`d-flex justify-content-center text-white`}>
            {/* {resultData ? resultData.data.result[0]} */}

            {(resultData?.data.result.data.data || []).map(
              (item: any, index: number) => {
                return (
                  <div
                    key={index}
                    className={`text-center ${styles.box_child_our_results}`}
                  >
                    <h1 className={styles.h1_count}>{item.total}</h1>
                    <h3>{item.name}</h3>
                  </div>
                );
              }
            )}

            {/* <div className={`text-center ${styles.box_child_our_results}`}>
              <h1 className={styles.h1_count}>3</h1>
              <h3>Kerja Sama</h3>
            </div>
            <div className={`text-center ${styles.box_child_our_results}`}>
              <h1 className={styles.h1_count}>10</h1>
              <h3>Karyawan</h3>
            </div> */}
          </div>
        </div>
      </section>

      <section id="reason" className={`p-5 ${styles.section_odd_container}`}>
        <div className="container text-center">
          <h1>ALASAN PILIH KAMI</h1>
          <div className="hr_line m-auto mb-3"></div>
          <div className="row">
            <div className={`col-lg-4 text-center p-5`}>
              <div
                data-aos="fade-up"
                data-aos-duration="2000"
                className={`${styles.square_reason} p-4`}
              >
                <h1 className={styles.h1_reason}>
                  <BsFillGeoAltFill />
                </h1>

                <h3 className="mb-3">LOKASI YANG NYAMAN</h3>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Sapiente consequatur tempore quam aperiam, repudiandae
                  similique nulla laudantium voluptas fugit fugiat, reiciendis a
                  cum provident rerum minus saepe iusto, modi vitae?
                </p>
              </div>
            </div>
            <div className={`col-lg-4 text-center p-5`}>
              <div
                data-aos="fade-up"
                data-aos-duration="2000"
                className={`${styles.square_reason} p-4`}
              >
                <h1 className={styles.h1_reason}>
                  <BsPersonCheckFill />
                </h1>

                <h3 className="mb-3">DI BEKALI ORANG YANG BERKUALITAS</h3>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Sapiente consequatur tempore quam aperiam, repudiandae
                  similique nulla laudantium voluptas fugit fugiat, reiciendis a
                  cum provident rerum minus saepe iusto, modi vitae?
                </p>
              </div>
            </div>
            <div className={`col-lg-4 text-center p-5`}>
              <div
                data-aos="fade-up"
                data-aos-duration="2000"
                className={`${styles.square_reason} p-4`}
              >
                <h1 className={styles.h1_reason}>
                  <BsTools />
                </h1>

                <h3 className={"mb-3"}>FASILITAS YANG LENGKAP</h3>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Sapiente consequatur tempore quam aperiam, repudiandae
                  similique nulla laudantium voluptas fugit fugiat, reiciendis a
                  cum provident rerum minus saepe iusto, modi vitae?
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="mt-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div
                data-aos="fade-up"
                data-aos-duration="2000"
                className="d-flex justify-content-center"
              >
                <div className={`${styles.wrap_image_left_reason}`}>
                  <Zoom>
                    <Image
                      height={320}
                      width={320}
                      sizes="100%"
                      loader={() => dummy_image1}
                      src={dummy_image1}
                      className={`${styles.image_left_reason}`}
                      alt="Picture of the author"
                    />
                  </Zoom>
                </div>
                {/* <div className={`${styles.wrap_image_left_reason}`}>
                  <Zoom>
                    <Image
                      height={300}
                      width={320}
                      loader={() => dummy_image2}
                      src={dummy_image2}
                      className={`w-100 ${styles.image_right_reason}`}
                      alt="Picture of the author"
                    />
                  </Zoom>
                </div> */}
              </div>
            </div>
            <div className="col-lg-6">
              <div data-aos="fade-up" data-aos-duration="2000" className="p-3">
                <h1>PERTANYAAN UMUM DI TANYA OLEH PELANGGAN</h1>
                <div className="hr_line mb-4"></div>

                {listCollapseQuestions?.map((item, index) => {
                  index += 1;

                  return (
                    <>
                      <p key={index} className="d-inline-flex w-100 gap-1">
                        <button
                          className="btn btn-dark w-100"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target={`#qs${index}`}
                          aria-expanded="false"
                          aria-controls={`qs${index}`}
                          onClick={() => {
                            if (item.id === index) {
                              setListCollapseQuestions((prevList) =>
                                prevList.map((question) =>
                                  question.id === index
                                    ? { ...question, status: !question.status }
                                    : question
                                )
                              );
                            }
                          }}
                        >
                          {item.question}
                          {item.status && item.id == index ? (
                            <BsFillCaretDownFill className="ms-3" />
                          ) : (
                            <BsFillCaretRightFill className="ms-3" />
                          )}
                        </button>
                      </p>
                      <div className="mb-3 collapse" id={`qs${index}`}>
                        <div className="card card-body">{item.paragraph}</div>
                      </div>
                    </>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="testimonial" className="mt-5">
        <div className={`${styles.testimoni_container} p-5`}>
          <div className={`${styles.testimoni_overflow_container}`}></div>
          <div className="container">
            <div className="row">
              <div className="d-flex flex-column justify-content-center">
                <div
                  className={`${styles.heading_testimoni} m-auto p-3 mb-5 bg-dark text-center`}
                >
                  <h1>TESTIMONI</h1>
                  <div className="hr_line m-auto mb-3"></div>
                </div>

                <div>
                  <Carousel activeIndex={index} onSelect={handleSelect}>
                    {(testimoniesData?.data.result.data.data || []).map(
                      (item: any, index: number) => {
                        return (
                          <Carousel.Item key={index}>
                            <div className="row">
                              <div className="col-lg-12">
                                <div className="bg-dark m-1 p-5">
                                  <div
                                    className={`${styles.image_parent_testimoni} d-flex justify-content-center`}
                                  >
                                    <Image
                                      loader={() =>
                                        process.env.NEXT_PUBLIC_BE_URL +
                                        "/storage/testimonies/" +
                                        item.image
                                      }
                                      height={200}
                                      width={200}
                                      src={`${process.env.NEXT_PUBLIC_BE_URL}/storage/testimonies/${item.image}`}
                                      alt="Picture of the author"
                                      className={`w-20 mb-5`}
                                    />
                                  </div>
                                  <div className="text-center">
                                    <h3 className="fs-2">{item.name}</h3>
                                    <h3 className="text-danger">
                                      {item.position}
                                    </h3>
                                    <p className="fs-4">
                                      &quot; {item.feedback} &quot;
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Carousel.Item>
                        );
                      }
                    )}
                  </Carousel>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="gallery" className={`p-5 ${styles.section_odd_container}`}>
        <div>
          <div className="text-center">
            <h1>GALERI KAMI</h1>
            <div className="hr_line m-auto mb-3"></div>
          </div>
          <div className="mt-5">
            <div
              data-aos="fade-up"
              data-aos-duration="2000"
              className={`d-flex justify-content-center ${styles.square_gallery_home}`}
            >
              {(galleryData?.data.result.data.data || [])?.map(
                (item: any, index: number): any => {
                  if (index <= 10)
                    return (
                      <div
                        key={index}
                        className={`p-3 ${styles.image_gallery}`}
                      >
                        <Zoom
                          IconZoom={CustomZoomInIcon}
                          // isZoomed={false} // Set the initial zoom state as needed
                          // onZoomChange={(value: any) => {
                          //   console.log("Zoom state changed:", value);
                          //   // Add any custom logic you need when zoom state changes
                          // }}
                        >
                          <Image
                            loader={() =>
                              process.env.NEXT_PUBLIC_BE_URL +
                              "/storage/galleries/" +
                              item.image
                            }
                            height={200}
                            width={200}
                            src={`${process.env.NEXT_PUBLIC_BE_URL}/storage/galleries/${item.image}`}
                            alt="Picture of the author"
                            className="w-100"
                          />
                        </Zoom>
                      </div>
                    );
                }
              )}
            </div>
          </div>

          <div className="d-flex justify-content-center mt-3">
            <button
              onClick={() => router.push("/gallery")}
              className={`${styles.button_part} btn btn-dark p-3 mt-3`}
            >
              <BsSearch className="me-3 mb-1" />
              Selengkapnya
            </button>
          </div>
        </div>
      </section>

      <section>
        <div data-aos="fade-up" data-aos-duration="2000">
          <div className="w-100 bg-dark text-white text-center p-5">
            <h1>JANGAN RAGU DATANGKAN MOBIL MU SEKARANG DI FINSGARAGE </h1>
          </div>
        </div>
      </section>

      <section id="contact" className="mt-5">
        <div className="container">
          <div>
            <div className="text-center">
              <h1>KONTAK</h1>
              <div className="hr_line m-auto mb-3"></div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <div
                data-aos="fade-up"
                data-aos-duration="2000"
                className="d-flex justify-content-center"
              >
                <iframe
                  width="600"
                  height="450"
                  src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15861.634597944403!2d106.7869415!3d-6.3410868!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69efa270c1437d%3A0x47ebb8873ea1e494!2sFins%20Garage!5e0!3m2!1sid!2sid!4v1697185079507!5m2!1sid!2sid"
                  allowFullScreen={true}
                  referrerPolicy="no-referrer-when-downgrade"
                  className="mt-5"
                />
              </div>
            </div>
            <div className="col-lg-6">
              <div className="m-1">
                <form
                  data-aos="fade-up"
                  data-aos-duration="2000"
                  className="mt-4 fs-3"
                >
                  <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">
                      Nama :
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Isi nama"
                      required
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">
                      Email :
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      id="exampleInputEmail1"
                      placeholder="Isi email"
                      aria-describedby="emailHelp"
                      required
                    />
                    <div id="emailHelp" className="form-text"></div>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="floatingTextarea">Pesan :</label>
                    <textarea
                      className={`form-control ${styles.text_area_contact}`}
                      placeholder="Isi pesan"
                      id="floatingTextarea"
                      required
                    ></textarea>
                  </div>

                  <button
                    // onClick={() => router.push("/gallery")}
                    className={`btn btn-dark p-2 mt-3 w-100`}
                  >
                    KIRIM
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Whatsapp />
    </div>
  );
}
