// import { HeadMetaData } from "@/components/HeadMetaData";
import NextDocument, { Html, Head, Main, NextScript } from "next/document";

const title =
  "Tempat retorasi dan perbaikan yang sangat rekomendasi untuk BMW E30 di Indonesia";
const defaultTitle = "Finsgarage";
const metaDescription =
  "Tempat retorasi dan perbaikan yang sangat rekomendasi untuk BMW E30 di Indonesia";
const ogImageUrl =
  "https://dl.dropboxusercontent.com/scl/fi/wthnua96pbktmc7psas7e/FINSgarage-new-logo-2021-3.png?rlkey=zgokm3k2nyrwz5hk4xq342iyv&dl=0";
const pageUrl = "http://localhost:3000";

export default class Document extends NextDocument {
  render() {
    return (
      <Html lang="en">
        <Head>
          <link rel="shortcut icon" href={ogImageUrl} />

          <meta name="title" content={title + " | " + defaultTitle} />
          <meta name="description" content={metaDescription} />
          <meta name="og:image" itemProp="image" content={ogImageUrl} />
          <meta property="og:url" content={pageUrl} />

          <meta property="og:type" content="website" />
          <meta property="og:image" itemProp="image" content={ogImageUrl} />
          <meta property="og:title" content={title + " | " + defaultTitle} />
          <meta property="og:description" content={metaDescription} />

          <meta name="twitter:card" content="summary_large_image" />
          <meta property="twitter:url" content={pageUrl} />
          <meta name="twitter:title" content={title + " | " + defaultTitle} />
          <meta name="twitter:image" content={ogImageUrl} />
          <meta property="twitter:description" content={metaDescription} />
        </Head>

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
