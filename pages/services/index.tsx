import { Jumbotron } from "@/components";
import React, { useLayoutEffect, useState } from "react";
import {
  BsArrowCounterclockwise,
  BsTools,
  BsPaletteFill,
  BsWhatsapp,
} from "react-icons/bs";
import styles from "@/styles/services.module.css";

export default function Index() {
  const [scrollY, setScrollY] = useState(0);
  const [windowSize, setWindowSize] = useState<TWindowSize>({
    height: 0,
    width: 0,
  });

  useLayoutEffect(() => {
    window.addEventListener("scroll", () => {
      const scrolled = window.scrollY;
      setScrollY(scrolled);
    });

    function handleResize() {
      const widthWindows = window.innerWidth;
      const heightWindows = window.innerHeight;

      setWindowSize({
        width: widthWindows,
        height: heightWindows,
      });
    }

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [scrollY, windowSize]);

  return (
    <div className="container_page">
      <Jumbotron />

      <section className={`p-5 ${styles.container_services}`}>
        <div className="container">
          <div className="text-center">
            <h1>JASA YANG KAMI SEDIAKAN</h1>
            <div className="hr_line m-auto"></div>
          </div>

          <div
            data-aos="fade-up"
            data-aos-duration="2000"
            className={`d-flex justify-content-center text-center mt-5 ${styles.parent_box_services}`}
          >
            <div className={`p-4 ${styles.square_services}`}>
              <BsArrowCounterclockwise
                className={`fs-1 ${styles.logo_square_services}`}
              />
              <h3 className="mt-4">Car Restoration</h3>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Perferendis ipsa dolore commodi sit voluptatibus eaque iure
                fugit sunt ipsum minus, id nam nobis accusamus at perspiciatis
                quas officia. Dolore, quas!
              </p>
            </div>
            <div className={`p-4 ${styles.square_services}`}>
              <BsTools className={`fs-1 ${styles.logo_square_services}`} />
              <h3 className="mt-4">Body Repair</h3>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Perferendis ipsa dolore commodi sit voluptatibus eaque iure
                fugit sunt ipsum minus, id nam nobis accusamus at perspiciatis
                quas officia. Dolore, quas!
              </p>
            </div>
            <div className={`p-4 ${styles.square_services}`}>
              <BsPaletteFill
                className={`fs-1 ${styles.logo_square_services}`}
              />
              <h3 className="mt-4">Car Painting</h3>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Perferendis ipsa dolore commodi sit voluptatibus eaque iure
                fugit sunt ipsum minus, id nam nobis accusamus at perspiciatis
                quas officia. Dolore, quas!
              </p>
            </div>
          </div>
        </div>
      </section>

      <section className={`${styles.container_how_it_works}`}>
        <div className="container">
          <div data-aos="fade-up" data-aos-duration="2000" className={`p-2`}>
            <div className="text-center w-100 d-flex justify-content-center">
              <div>
                <h1>CARA KERJA</h1>
                <div className="hr_line m-auto"></div>
              </div>
            </div>

            <div>
              <div className="d-flex mt-5">
                <div>
                  <div
                    className={`d-flex flex-column justify-content-end p-3 ${styles.square_how_it_works_left}`}
                  >
                    <div className="d-flex flex-column w-100 justify-content-end text-end me-5">
                      <div className="w-100 d-flex justify-content-end align-items-center p-2">
                        <div className="bg-danger text-white text-center">
                          <p className="p-3 fs-2">1</p>
                        </div>
                      </div>
                      <div className="mt-3">
                        <h4 className="fs-4">Hubungi</h4>
                        <p>
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Tempore, assumenda consequuntur. Id unde nam
                          asperiores quam mollitia in, aperiam accusamus at
                          quibusdam repellat quia aut doloremque. Distinctio quo
                          consequatur nam?
                        </p>
                      </div>
                    </div>
                  </div>
                </div>

                <span
                  className={`border ${
                    windowSize.width < 768
                      ? scrollY < 2352 && "border-danger"
                      : scrollY < 1902 && "border-danger"
                  }`}
                >
                  {" "}
                </span>

                <div
                  className={`d-flex justify-content-end p-3 ${styles.square_how_it_works_right}`}
                >
                  <div className="d-flex flex-column w-100 justify-content-end text-left">
                    <div className="w-100 d-flex justify-content-start align-items-center p-2">
                      <div className="bg-danger text-white text-center">
                        <p className="p-3 fs-2">2</p>
                      </div>
                    </div>
                    <div className="mt-3">
                      <h4 className="fs-4">Booking</h4>
                      <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing
                        elit. Tempore, assumenda consequuntur. Id unde nam
                        asperiores quam mollitia in, aperiam accusamus at
                        quibusdam repellat quia aut doloremque. Distinctio quo
                        consequatur nam?
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div className="d-flex">
                <div
                  onClick={() => {
                    console.log(scrollY);
                  }}
                >
                  <div
                    className={`d-flex flex-column justify-content-end p-3 ${styles.square_how_it_works_left}`}
                  >
                    <div className="d-flex flex-column w-100 justify-content-end text-end me-5">
                      <div className="w-100 d-flex justify-content-end align-items-center p-2">
                        <div className="bg-danger text-white text-center">
                          <p className="p-3 fs-2">3</p>
                        </div>
                      </div>
                      <div className="mt-3">
                        <h4 className="fs-4">Berikan</h4>
                        <p>
                          Lorem ipsum dolor sit, amet consectetur adipisicing
                          elit. Tempore, assumenda consequuntur. Id unde nam
                          asperiores quam mollitia in, aperiam accusamus at
                          quibusdam repellat quia aut doloremque. Distinctio quo
                          consequatur nam?
                        </p>
                      </div>
                    </div>
                  </div>
                </div>

                <span className={`border ${scrollY > 1902 && "border-danger"}`}>
                  {" "}
                </span>
                <div
                  className={`d-flex justify-content-end p-3  ${styles.square_how_it_works_right}`}
                >
                  <div className="d-flex flex-column w-100 justify-content-end text-left">
                    <div className="w-100 d-flex justify-content-start align-items-center p-2">
                      <div className="bg-danger text-white text-center">
                        <p className="p-3 fs-2">4</p>
                      </div>
                    </div>
                    <div className="mt-3">
                      <h4 className="fs-4">Ambil</h4>
                      <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing
                        elit. Tempore, assumenda consequuntur. Id unde nam
                        asperiores quam mollitia in, aperiam accusamus at
                        quibusdam repellat quia aut doloremque. Distinctio quo
                        consequatur nam?
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* <div className="d-flex justify-content-around m-5">
            <div className="d-flex justify-content-center align-items-center bg-white border m-4">
              <div className="h-100 border border-danger d-flex justify-content-center align-items-center bg-danger text-white">
                <p className="fs-2 p-4 mt-1">1</p>
              </div>
              <div className="mt-3 p-4">
                <p className="fs-4">Hubungi</p>
              </div>
            </div>

            <div className="d-flex justify-content-center align-items-center border m-4">
              <div className="h-100 border border-danger d-flex justify-content-center align-items-center bg-danger text-white ">
                <p className="fs-2 p-4 mt-1">2</p>
              </div>
              <div className="p-4 mt-3">
                <p className="fs-4">Booking</p>
              </div>
            </div>

            <div className="d-flex justify-content-center align-items-center border m-4">
              <div className="h-100 border border-danger d-flex justify-content-center align-items-center bg-danger text-white">
                <p className="fs-2 p-4 mt-1">3</p>
              </div>
              <div className="p-4 mt-3">
                <p className="fs-4">Berikan</p>
              </div>
            </div>

            <div className="d-flex justify-content-center align-items-center border  m-4">
              <div className="h-100 border border-danger d-flex justify-content-center align-items-center bg-danger text-white">
                <p className="fs-2 p-4 mt-1">4</p>
              </div>
              <div className="p-4 mt-3">
                <p className="fs-4">Ambil</p>
              </div>
            </div>
          </div> */}
          </div>
        </div>
      </section>

      <section className="mt-4">
        <div className={`container ${styles.contact_services}`}>
          <div className={`${styles.contact_overlay}`}></div>
          <div
            className={`d-flex text-white justify-content-center flex-column align-items-center ${styles.contact_content}`}
          >
            <h2>Hubungi Sekarang</h2>
            <button className={`${styles.button_part} btn btn-dark p-3 mt-3`}>
              <BsWhatsapp className="me-3 fs-5" />
              Whatsapp
            </button>
          </div>
        </div>
      </section>
    </div>
  );
}
