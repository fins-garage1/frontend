import * as AWS from "aws-sdk";

AWS.config.update({
  signatureVersion: "v4",
  accessKeyId: process.env.NEXT_PUBLIC_AWS_KEY,
  secretAccessKey: process.env.NEXT_PUBLIC_AWS_SECRET,
});

const s3 = new AWS.S3();

export async function getFileUrlsFromS3(
  bucketName: string = "fg-project",
  expirySeconds: number = 3600
) {
  try {
    const params = {  
      Bucket: bucketName,
    };

    const data = await s3.listObjectsV2(params).promise();
    const urls: { [key: string]: string } = {};

    for (const item of data.Contents || []) {
      let key = item.Key || "";

      if (key) {
        const parts = key.split("/");
        const fileName = parts[parts.length - 1];

        const urlParams = {
          Bucket: bucketName,
          Key: key,
          Expires: expirySeconds,
        };

        const url = await s3.getSignedUrlPromise("getObject", urlParams);
        urls[fileName] = url;
      }
    }

    return urls;
  } catch (err) {
    console.error("Error getting file URLs from S3:", err);
    throw err;
  }
}

export async function getFileUrlFromS3(
  bucketName: string,
  fileName: string,
  expirySeconds: number = 3600
) {
  try {
    const params = {
      Bucket: bucketName,
      Key: fileName,
      Expires: expirySeconds,
    };

    const url = await s3.getSignedUrl("getObject", params);
    console.log("File URL:", url);
    return url;
  } catch (err) {
    console.error("Error getting file URL from S3:", err);
    throw err;
  }
}

export async function listBucketContents(bucketName: string) {
  try {
    const data = await s3.listObjectsV2({ Bucket: bucketName }).promise();
    console.log(`Contents of S3 Bucket '${bucketName}':`);

    if (data.Contents) {
      data.Contents.forEach((object) => {
        console.log("-", object.Key);
      });
    } else {
      console.log("Bucket is not exist");
    }
  } catch (err) {
    console.error(`Error listing contents of S3 bucket '${bucketName}':`, err);
  }
}
