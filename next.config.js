/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ["fg-project.s3.eu-north-1.amazonaws.com"],
  },
  reactStrictMode: true,
};

module.exports = nextConfig;
