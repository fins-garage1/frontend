type TListNavigator = {
  name: string;
  link: string;
};

type TWindowSize = {
  height: number;
  width: number;
};

type TListCollapseQuestion = {
  id: number;
  question: string;
  paragraph: string;
  collapse: number;
  status: boolean;
};

type TOpenTime = {
  id: number;
  day: string;
  open_time: string;
};
