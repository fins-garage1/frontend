export function findDuplicates(nums: number[]): number[] {
  nums.sort(); // alters original array
  let ans = [];

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] === nums[i + 1]) {
      if (ans[ans.length - 1] !== nums[i]) {
        ans.push(nums[i]);
      }
    }
  }
  return ans;
}
