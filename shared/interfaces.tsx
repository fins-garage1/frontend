interface IPagination {
    data: any[],
    limit: number,
}