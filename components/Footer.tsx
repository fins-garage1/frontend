import React from "react";
import styles from "../styles/footer.module.css";
import Image from "next/image";
import Logo from "@/images/finsgarage.png";
import { BsYoutube, BsFacebook, BsInstagram } from "react-icons/bs";
import Link from "next/link";

export default function Footer() {
  return (
    <footer
      className={`${styles.container_footer} mt-5 bg-dark fixed text-white w-100`}
    >
      <div>
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div
                className={`d-flex justify-content-between ${styles.left_footer_container} p-5`}
              >
                <div>
                  <div>
                    <Image
                      src={Logo}
                      height={40}
                      alt="Picture of the author"
                      className="mt-4 mb-4"
                    />
                    <p className={`${styles.left_p}`}>
                      Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                      Ullam fuga consectetur beatae ipsa. Quas mollitia magnam
                      rem ut assumenda eaque provident eos dolor unde dolores?
                      Fuga deserunt possimus esse consequatur?
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3">
              <div className={`d-flex ${styles.right_footer_container} p-5`}>
                <div className="pe-5">
                  <h3>INFORMASI</h3>
                  <div className="w-50 hr_line mb-3"></div>
                  <p>
                    <b>Alamat :</b>
                    <br />
                    Jl. PLN No.54, Gandul, Kec. Cinere, <br />
                    Kota Depok, Jawa Barat 16513
                  </p>
                  <p>
                    <b>Jam Buka :</b>
                    <br /> Senin 09.00 - 17.00 <br />
                    Selasa 09.00 - 17.00 <br />
                    Rabu 09.00 - 17.00 <br />
                    Kamis 09.00 - 17.00 <br /> Jumat 09.00 - 17.00 <br /> Sabtu
                    09.00 - 17.00 <br />
                    Minggu Tutup
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-3">
              <div className="p-5">
                <h3>SOCIAL LINK</h3>
                <div className="hr_line mb-4"></div>
                <div className="d-flex flex-wrap">
                  <Link className={`${styles.link_logo_footer} me-3`} href="/">
                    <BsInstagram className="fs-4" />
                  </Link>
                  <Link className={`${styles.link_logo_footer} me-3 `} href="/">
                    <BsFacebook className="fs-4" />
                  </Link>
                  <Link className={`${styles.link_logo_footer} me-3`} href="/">
                    <BsYoutube className="fs-4" />
                  </Link>
                </div>
              </div>
            </div>

            <hr className="bg-white" />
            <div className="pb-2 text-center">
              <p>&copy; FINSGARAGE</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
