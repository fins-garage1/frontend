import React, { useCallback, useEffect, useState } from "react";
import styles from "@/styles/home.module.css";
import Image from "next/image";
import { Carousel, Col, Container, Row } from "react-bootstrap";
import Slideshow1 from "@/images/slideshow/1.jpg";
import Slideshow2 from "@/images/slideshow/2.jpg";
import Slideshow3 from "@/images/slideshow/3.jpg";
import { BsClockFill, BsFillGeoAltFill } from "react-icons/bs";
import data_open_time from "@/json/list_open_time.json";

export default function Slideshow() {
  const [dataSlideShow, setDataSlideShow] = useState([
    Slideshow1,
    Slideshow2,
    Slideshow3,
  ]);
  // const [currentTime, setCurrentTime] = useState(new Date());
  const [openTime, setOpenTIme] = useState<TOpenTime[]>([...data_open_time]);
  const currentTime = openTime[new Date().getDay()];
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex: any): any => {
    setIndex(selectedIndex);
  };

  const CurrentTime = useCallback(() => {
    const time = new Date();

    return <div>{time.toLocaleTimeString()}</div>;
  }, []);

  return (
    <Carousel activeIndex={index} onSelect={handleSelect} interval={2000}>
      {dataSlideShow?.map((item: any, index: number): any => {
        return (
          <Carousel.Item key={index}>
            <div className={styles.image_slideshow_container}>
              <div className={styles.image_slideshow_overlay}></div>
              <Image
                src={item}
                alt="Picture of the author"
                className={`img-fluid ${styles.image_slideshow}`}
                layout="responsive"
              />
            </div>

            <Carousel.Caption className={`${styles.carousel_caption}`}>
              <div data-aos="fade-up" data-aos-duration="3100">
                <h1 className={`${styles.header_welcome_page}`}>
                  SELAMAT DATANG DI WEBSITE FINSGARAGE
                </h1>
                <p className={`${styles.slide_show_message}`}>
                  Melayani Restorasi dan perbaikan untuk BMW E30
                </p>
                <hr />
                <div className="d-flex justify-content-center">
                  <div className="d-flex justify-content-center me-3">
                    <p className="fs-3 me-2">
                      <BsClockFill />
                    </p>
                    <p className="fs-3">{currentTime.open_time}</p>
                  </div>

                  <p className="fs-3">|</p>

                  <div className="d-flex justify-content-center ms-3">
                    <p className="fs-4">
                      <BsFillGeoAltFill className="me-2" />
                    </p>
                    <p className="fs-3">Jakarta</p>
                  </div>
                </div>
              </div>
            </Carousel.Caption>
          </Carousel.Item>
        );
      })}
    </Carousel>
  );
}
