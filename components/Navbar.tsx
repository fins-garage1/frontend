import React, { useState, useRef, useMemo, useLayoutEffect } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import styles from "@/styles/navbar.module.css";
import { Nav, Container, Navbar as Navbrand } from "react-bootstrap";
import data_navigator from "@/json/list_navigator.json";

interface NavbarProps {
  files: Record<string, string>;
}

export default function Navbar({ files }: NavbarProps) {
  const router = useRouter();
  const select = useRef("HOME");
  const [scrollY, setScrollY] = useState(0);
  const [windowSize, setWindowSize] = useState<TWindowSize>({
    height: 0,
    width: 0,
  });
  const [listNavigator, setListNavigator] = useState<TListNavigator[]>([
    ...data_navigator,
  ]);

  useLayoutEffect(() => {
    window.addEventListener("scroll", () => {
      const scrolled = window.scrollY;
      setScrollY(scrolled);
    });

    function handleResize() {
      const widthWindows = window.innerWidth;
      const heightWindows = window.innerHeight;

      setWindowSize({
        width: widthWindows,
        height: heightWindows,
      });
    }

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [scrollY, windowSize]);

  const handleSelectNavigator = useMemo(() => {
    return (name: string, link: string) => {
      select.current = name;

      router.push(link);
    };
  }, [select, router]);

  const stylingDynamicNavbar =
    scrollY <= 200 && (windowSize.width > 700 || windowSize.width == 0)
      ? `${styles.transparent} p-5`
      : "bg-dark";

  return (
    <>
      <Navbrand
        expand="lg"
        className={`${stylingDynamicNavbar} fixed-top ${styles.navbar}`}
      >
        <Container className={`p-2 ${styles.container_navbar}`}>
          <Navbrand.Brand href="/">
            <Image
              src={files["finsgarage.png"]}
              width={140}
              height={40}
              alt=""
            />
          </Navbrand.Brand>
          <Navbrand.Toggle
            className={` ${styles.button_navbar_collapse}`}
            aria-controls="basic-navbar-nav"
          />
          <Navbrand.Collapse id="basic-navbar-nav">
            <Nav className={`ms-auto`}>
              {listNavigator?.map((item: any, index: any): any => {
                if (select.current === item.name) {
                  return (
                    <Nav.Link
                      onClick={() =>
                        handleSelectNavigator(item.name, item.link)
                      }
                      className={`${styles.active_nav_link}`}
                      key={index}
                    >
                      {item.name}
                    </Nav.Link>
                  );
                } else {
                  return (
                    <Nav.Link
                      onClick={() =>
                        handleSelectNavigator(item.name, item.link)
                      }
                      className={styles.nav_link}
                      key={index}
                    >
                      {item.name}
                    </Nav.Link>
                  );
                }
              })}
            </Nav>
          </Navbrand.Collapse>
        </Container>
      </Navbrand>
    </>
  );
}
