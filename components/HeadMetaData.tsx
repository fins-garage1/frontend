import React from "react";
import Head from "next/head";
// import Logo from "@/images/finsgarage.png";

export const HeadMetaData: React.FC<{
  title?: string;
  metaDescription?: string;
  ogImageUrl?: string;
  pathname?: string;
}> = ({
  title = "Finsgarage, tempat retorasi dan perbaikan yang sangat rekomendasi untuk BMW E30 di Indonesia",
  metaDescription,
  ogImageUrl = "https://www.dropbox.com/scl/fi/wthnua96pbktmc7psas7e/FINSgarage-new-logo-2021-3.png?rlkey=zgokm3k2nyrwz5hk4xq342iyv&dl=0",
  pathname = "",
}) => {
  const defaultTitle = "Finsgarage";

  const baseUrl =
    // process.env.NODE_ENV === "development"
    /*?*/ "http://localhost:3000";
  //   : "https://v6-academy.com";

  const pageUrl = new URL(pathname, baseUrl).toString();

  return (
    <Head>
      <title>{title + " | " + defaultTitle}</title>

      <meta name="title" content={title + " | " + defaultTitle} />
      <meta name="description" content={metaDescription} />
      <meta name="og:image" itemProp="image" content={ogImageUrl} />
      <meta property="og:url" content={pageUrl} />

      <meta property="og:type" content="website" />
      <meta property="og:image" itemProp="image" content={ogImageUrl} />
      <meta property="og:title" content={title + " | " + defaultTitle} />
      <meta property="og:description" content={metaDescription} />

      <meta name="twitter:card" content="summary_large_image" />
      <meta property="twitter:url" content={pageUrl} />
      <meta name="twitter:title" content={title + " | " + defaultTitle} />
      <meta name="twitter:image" content={ogImageUrl} />
      <meta property="twitter:description" content={metaDescription} />
    </Head>
  );
};
