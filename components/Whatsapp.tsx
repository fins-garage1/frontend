import React from "react";
import { BsWhatsapp } from "react-icons/bs";

export default function Whatsapp() {
  return (
    <div className="fixed-bottom">
      <div>
        <div className="bg-dark text-white d-flex justify-content-center align-items-center text-center" style={{ cursor: 'pointer' }}>
          <BsWhatsapp className="fs-3" />
          <p className="ms-3 mt-3 fs-3">Hubungi Sekarang</p>
        </div>
      </div>
    </div>
  );
}
