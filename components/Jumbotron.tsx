import React, { useState } from "react";
import styles from "@/styles/jumbotron.module.css";
import { BsClockFill, BsFillGeoAltFill } from "react-icons/bs";
import data_open_time from "@/json/list_open_time.json";

export default function Jumbotron() {
  const [openTime, setOpenTIme] = useState<TOpenTime[]>([...data_open_time]);
  const currentTime = openTime[new Date().getDay()];

  return (
    <div className={styles.container_jumbotron}>
      <div className={`${styles.image_slideshow_overlay}`}></div> {/* Add this overlay */}
      <div
        className={`${styles.container_text_jumbotron} text-center`}
        data-aos="fade-up"
        data-aos-duration="3100"
      >
        <h1 className={`${styles.header_welcome_page}`}>
          SELAMAT DATANG DI WEBSITE FINSGARAGE
        </h1>
        <p className={`${styles.slide_show_message}`}>
          Melayani Restorasi dan perbaikan untuk BMW E30
        </p>
        <hr />
        <div className="d-flex justify-content-center">
          <div className="d-flex justify-content-center me-3">
            <p className="fs-3 me-2">
              <BsClockFill />
            </p>
            <p className="fs-3">{currentTime.open_time}</p>
          </div>

          <p className="fs-3">|</p>

          <div className="d-flex justify-content-center ms-3">
            <p className="fs-4">
              <BsFillGeoAltFill className="me-2" />
            </p>
            <p className="fs-3">Jakarta</p>
          </div>
        </div>
      </div>
    </div>
  );
}
