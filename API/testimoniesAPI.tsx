import axios from "axios";

export async function listTestimonies(api: string) {
  return axios.get(`${process.env.NEXT_PUBLIC_BE_URL}/core/testimoni/get`, {
    params: { api: api },
  });
}
