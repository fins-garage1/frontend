import axios from "axios";

export async function listGalleries(api: string, limit: number) {
  return axios.get(`${process.env.NEXT_PUBLIC_BE_URL}/core/gallery/get`, {
    params: { api: api, limit },
  });
}
