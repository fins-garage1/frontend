import axios from "axios";

export async function listResults(api: string) {
  return axios.get(`${process.env.NEXT_PUBLIC_BE_URL}/core/result/get`, {
    params: { api: api },
  });
}
