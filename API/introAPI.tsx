import axios from "axios";

export async function listIntroduction(api: string) {
  return axios.get(`${process.env.NEXT_PUBLIC_BE_URL}/core/introduction/get`, {
    params: { api: api },
  });
}
